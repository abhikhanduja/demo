//
//  ViewController2.swift
//  DemoProj
//
//  Created by abhinav khanduja on 15/12/19.
//  Copyright © 2019 abhinav khanduja. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    @IBOutlet weak var priceRangeBar: PriceRangeBarView!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button.layer.cornerRadius = button.frame.height/2
        button.addTarget(self, action: #selector(buttontouched(_:)), for: .touchDown)
//        button.setBackgroundColor(color: .red, forState: .highlighted)
    }
    
    @objc func buttontouched(_ sender: UIButton) {
        button.backgroundColor = .red
    }
    
    @IBAction func btnTapped(_ sender: UIButton) {
        button.backgroundColor = .clear
    }

}
