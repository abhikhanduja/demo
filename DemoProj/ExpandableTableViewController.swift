
//
//  ExpandableTableViewController.swift
//  DemoProj
//
//  Created by abhinav khanduja on 01/12/19.
//  Copyright © 2019 abhinav khanduja. All rights reserved.
//

import UIKit

class ExpandableTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var array = ["bbb"]
    
    @IBOutlet weak var tableview: UITableView!
    
    var viewBottomYPoint : CGFloat {
        get {
            return (view.frame.origin.y + view.frame.height)
        }
    }
    
    var tableviewBottomYPoint : CGFloat {
        get {
            return (tableview.frame.origin.y + tableview.frame.height)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.layer.cornerRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableview.frame.size.height = tableview.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "expCell", for: indexPath)
        cell.textLabel?.text = "\(array[indexPath.row]) " + "\(indexPath.row)"
        return cell
    }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        array.append("bbb")
        tableview.reloadData()
        if viewBottomYPoint - tableviewBottomYPoint < 10 {
            let indexPath = IndexPath(row: array.count - 1, section: 0)
            tableview.scrollToRow(at: indexPath, at: .bottom, animated: true)
            return
        }
        tableview.frame.size.height = tableview.contentSize.height
    }

}
