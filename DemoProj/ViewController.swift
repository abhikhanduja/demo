//
//  ViewController.swift
//  DemoProj
//
//  Created by abhinav khanduja on 26/11/19.
//  Copyright © 2019 abhinav khanduja. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var counterButton: UIButton!
    @IBOutlet weak var tctField: UITextField!
    @IBOutlet weak var shapeView: UIView!
    
    let locMgr = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Calendar.current.dateComponents([.weekday, .day], from: Date()))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        counterButton.layer.cornerRadius = counterButton.frame.width/2
        counterButton.backgroundColor = .white
        circularProress()
    }
    
    @objc func btnTouchedDown() {
        self.counterButton.backgroundColor = UIColor.lightGray
    }
    
    func circularProress() {
        let circularPath = UIBezierPath(arcCenter: .zero, radius: counterButton.frame.width/2, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true)
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeEnd = 0
        shapeLayer.lineCap = .round
        shapeLayer.position = CGPoint(x: counterButton.frame.width/2, y: counterButton.frame.height/2)
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi/2, 0, 0, 1)
        counterButton.layer.addSublayer(shapeLayer)
        
        let basicAnim = CABasicAnimation(keyPath: "strokeEnd")
        basicAnim.toValue = 1
        basicAnim.duration = 2
        basicAnim.fillMode = CAMediaTimingFillMode.forwards
        basicAnim.isRemovedOnCompletion = false
        shapeLayer.add(basicAnim, forKey: "bacicKey")
    }
    
    func setShape() {
        let curveStrtPt : CGFloat = shapeView.frame.width/2 - 20.0
        let viewOrigin = shapeView.frame.origin
        let arcCenter = CGPoint(x: CGFloat(shapeView.frame.width/2), y: viewOrigin.y)
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: curveStrtPt, y: 0))
//        path.addCurve(to: CGPoint(x: shapeView.frame.width/2 + 20.0, y: viewOrigin.y), controlPoint1: CGPoint(x: shapeView.frame.width, y: viewOrigin.y + 20), controlPoint2: CGPoint(x: shapeView.frame.width/2 + 20.0, y: viewOrigin.y))
        
//        path.addQuadCurve(to: CGPoint(x: curveStrtPt + 5, y: 5), controlPoint: CGPoint(x: curveStrtPt + 10, y: 2.5))
        path.addCurve(to: CGPoint(x: curveStrtPt + 40, y: 0), controlPoint1: CGPoint(x: curveStrtPt, y: 5), controlPoint2: CGPoint(x: curveStrtPt + 20, y: 5))
        
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        layer.fillColor = UIColor.red.cgColor
        layer.strokeColor = UIColor.red.cgColor
        layer.strokeEnd = 1
        shapeView.layer.addSublayer(layer)
        
        shapeView.layer.addSublayer(layer)
    }
    
    func setLocMgr() {
        locMgr.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkForAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            print(locMgr.location?.coordinate)
            break
        case .denied:
            break
        case .notDetermined:
            locMgr.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        case .authorizedAlways:
            break
        default:
            break
        }
    }

    @IBAction func btnTapped(_ sender: UIButton) {
        sender.isSelected = true
        if sender.state == .selected {
            sender.isSelected = false
        }
        
//        if CLLocationManager.locationServicesEnabled() {
//            setLocMgr()
//            checkForAuthorization()
//        }
    }
    
}

