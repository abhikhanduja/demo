//
//  CustomView.swift
//  DemoProj
//
//  Created by abhinav khanduja on 28/11/19.
//  Copyright © 2019 abhinav khanduja. All rights reserved.
//

import UIKit

@IBDesignable class CustomView: UIView {

    @IBInspectable private var borderWidth : CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable private var borderColor : UIColor = UIColor.black {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable private var cornerRadius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}
