//
//  PriceRangeBarView.swift
//  DemoProj
//
//  Created by abhinav khanduja on 15/12/19.
//  Copyright © 2019 abhinav khanduja. All rights reserved.
//

import UIKit

class PriceRangeBarView: UIView {

    @IBOutlet var contentView: UIView!
    
    
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    
    private var leftViewOrigin : CGPoint!
    private var rightViewOrigin : CGPoint!
    
    var w : CGFloat!
    
    var minimum : Int!
    var maximum : Int!
    var difference : Int!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        leftViewOrigin = frame.origin
        rightViewOrigin = CGPoint(x: frame.width - rightView.frame.width, y: rightView.frame.origin.y)
        setView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        leftViewOrigin = leftView.frame.origin
        w = 0.6 * self.frame.height
        rightViewOrigin = CGPoint(x: self.frame.width - w, y: rightView.frame.origin.y)
    }
    
    private func setView() {
        Bundle.main.loadNibNamed("PriceRangeBarView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addPangesture(view: leftView)
        addPangesture(view: rightView)
        
        minimum = 0
        maximum = 10000
        difference = 500
    }
    
    private func addPangesture(view: UIView) {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        view.addGestureRecognizer(pan)
    }
    
    @objc private func handlePan(_ sender: UIPanGestureRecognizer) {
        let panView = sender.view
        
        let translation = sender.translation(in: self)
        
        switch sender.state {
        case .began, .changed:
            moveViews(translation: translation, panView: panView!)
            sender.setTranslation(.zero, in: self)
            break
        default:
            break
        }
        
    }
    
    private func moveViews(translation: CGPoint, panView: UIView) {
        let posMoved = translation.x
        panView.center.x = (panView == leftView) ? (leftView.center.x + posMoved) : (rightView.center.x + posMoved)
        
        if panView == leftView {
            if panView.frame.origin.x <= self.frame.origin.x {
                panView.frame.origin.x = leftViewOrigin.x
            }else if panView.center.x > (rightViewOrigin.x) {
                panView.frame.origin.x = rightViewOrigin.x - (w)
            }
        }
        
        if panView == rightView {
            if (panView.frame.origin.x) >= (rightViewOrigin.x) {
                panView.frame.origin.x = rightViewOrigin.x
            }else if panView.frame.origin.x < (leftViewOrigin.x + leftView.frame.width) {
                panView.frame.origin.x = leftViewOrigin.x + leftView.frame.width
            }
        }
    }
    
}
